type CarYear = number;
type CarType = string;
type Carmodel = string;
type Car = {
    year: CarYear,
    type: CarType,
    model: Carmodel
}
const carYear:CarYear = 2001;
const carType:CarType = "toyota";
const carModel:Carmodel = "corolla";
const car1:Car = {
    year:2001,
    type:"Nissan",
    model:"XXX",
}
console.log(car1);
