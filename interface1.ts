interface Ractangle {
    width:number;
    height:number;
}
interface ColoredRectangle extends Ractangle{
    color:string;
}
const rectangle:Ractangle = {
    width:20,
    height:10
}
console.log(rectangle);
const coloredRectangle:ColoredRectangle = {
    width:20,
    height:10,
    color:"red"
}
console.log(coloredRectangle);

